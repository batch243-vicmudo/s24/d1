//console.log("Good afternoon");

// [Section] Exponent operator

//before ES6
	const firstNum = 8 ** 2;
	console.log(firstNum);

// ES6
	const secondNum = Math.pow(8, 2);
	console.log(secondNum);

//[Section] Template Literals
/*
	allows to write strings without using concatenation operator(+)
	-greatly helps with code readability.
*/
	let name = 'John';
// before template literal string
// Use single quote or double quote
	let message = 'Hello ' + name + '! Welcome to programming!';
	console.log(message);
// Using template literal
// using backticks(``);
	message = `Hello
	${name}!!!!!! 
	Welcome to programming!`
	console.log(message);

	// Template literals allow us to write strings with embedded Javascript

	const insterestRate =0.1;
	const principal = 2000;
	console.log(`The interest on your savings account is: ${insterestRate*principal}.`)

//[Section] Array Destructuring
/*
	-allows us to unpack elements in arrays into distinct variables.
	-allows us to name array elements with variables instead of using index numbers.
	-it will help us with code readability
	-Syntax:
	let/const [variableNameA, variableNameB, .. ...]= arrayName;
*/

	const fullName = ['Juan', 'Dela', 'Cruz'];
	//Before array destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);
	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]} ! It's nice to meet you.`);

	//Array destructing
	const [firstName1, middleName1, lastName1] = fullName;
	console.log(firstName1);
	console.log(middleName1);
	console.log(lastName1);

	console.log(fullName);
	console.log(`Hello ${firstName1} ${middleName1} ${lastName1} ! It's nice to meet you.`);

	// [Section] Object Destructuring
	/*
		-allows us to unpack properties of objects into distinct variables.
		-Syntax:
		let/const {propertyNameA, propertyNameB,propertyNameC, . . ..} = objectName;
	*/

	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	};

	// Before the object destructuring
	console.log(person.givenName);
	console.log(person["maidenName"]);
	console.log(person.familyName);
	console.log("------------------------------------------");
	// Object Destructuring
	let {givenName, maidenName, familyName} = person;
	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);
	console.log("------------------------------------------");

	function getFullName ({givenName, maidenName, familyName}){
		console.log(`${givenName} ${maidenName} ${familyName}.`)
	}
	getFullName(person);
	console.log("------------------------------------------");
// [Section] Arrow Functions
	// Compact alternative syntax to traditional functions.
	// useful for code snippets where creating functions will not be reused in any other portion of the code

	const hello = () => console.log("Hello World!")

	/* function expression
	const hello = function(){
		console.log("Hello World!");
	}*/

	hello();
	console.log("------------------------------------------");

	// before arrow function and template literals

	//function declaration
	function printFullName(firstName, middleInitial, lastName){
		console.log(firstName + ' ' +middleInitial+ ' ' +lastName);
	}

	printFullName("Chris", "0.", "Mortel");

	let fullNewName = (firstName, middleInitial, lastName) => {
		console.log(`${firstName} ${middleInitial} ${lastName}`);
	}

	fullNewName("Chris", "0.", "Mortel");
	console.log("------------------------------------------");

	// Arrow functions with loops

	const student = ["John", "Jane", "Judy"];

	// before arrow function
	//arrayName.forEach(function(indivualElement){statement/s.})
		/*	
		student.forEach(function(studentName){
			console.log(studentName + " is a student!")
		});
		*/
	// arrow function

		student.forEach((student) => {
			console.log(`${student} is a student!`)
		});
	console.log("------------------------------------------");
	// [Section] Implicit Return Statement
		/*
			there are instances when you can omit return statement
			this works because even without return statement Javascript implicitly adds it for the result of the function
		*/

	const add = (x,y) => {
		//console.log(x+y);
		return (x+y);
	}

	let sum = add(23, 45);
	console.log("This is the sum contain in sum variable: ");
	console.log(sum);
	console.log("------------------------------------------");

	const subtract = (x, y) => x-y;
	subtract(10, 5);
	let difference = subtract(10, 5);
	console.log(difference);
	console.log("------------------------------------------");

	// [Section] Default function Argument Value
	// provide a default argument value if none if provide when the function is invoked.

	const greet = (name = 'User') =>{
		return `Good morning, ${name}!`;
	}

	console.log(greet());

	// [Section] Class-based Object Blueprints

	// Allows us to create/instantiation of objects using classes blueprints

	// Creating class
		//constructor is a special method of a class for creating/initializing an object for that class.
	/*
		-Syntax:
		class className{
			constructor (objectValueA, objectValueB, . . .){
			this.objectPropertyA = objectValueA;
			this.objectPropertyB = objectValueB;
			}
		}
	*/

	class Car{
		constructor(brand, name, year){
			this.carBrand = brand;
			this.carName = name;
			this.carYear = year;
		}
	}

	let car = new Car("Toyota", "Hilux-pickup", "2015");
	console.log(car);

	car.carBrand = "Nissan";
	console.log(car);
